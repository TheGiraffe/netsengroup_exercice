# README #

This repository has been created purely for learning intents in the context of an internship at NetsenGroup. 

It is heavily inspired from the following tutorials:

### For the backend

- [Getting Started with EF Core](https://docs.microsoft.com/en-us/ef/core/get-started/?tabs=netcore-cli)

### For the frontend

- [Build your first Xamarin.Forms App](https://docs.microsoft.com/en-us/xamarin/get-started/first-app/?pivots=windows)
- [Create a Single Page Xamarin.Forms Application](https://docs.microsoft.com/en-us/xamarin/get-started/quickstarts/single-page?pivots=windows)


## What is this repository for? ###

This repository is to be reviewed by NetsenGroup.

## How do I get set up? ###

### For the backend

#### Setup

* Install the required software if necessary:

	* .Net Core CLI from [here](https://www.microsoft.com/net/download/core)
	* Entity Framework Core from the CLI:

```
$ dotnet add package Microsoft.EntityFrameworkCore.Sqlite
```

#### How to run the sql database

Simply run this command from the the project folder (*/netsengroup_exercice/EFGetStarted*)

```
$ dotnet run
```

### For the frontend

* Install Visual Studio if necessary from [here](https://visualstudio.microsoft.com/fr/downloads/).
* open either of the following projects:

```
/netsengroup_exercice/AwesomeApp/AwesomeApp.sln
```

```
/netsengroup_exercice/Notes/Notes.sln
```