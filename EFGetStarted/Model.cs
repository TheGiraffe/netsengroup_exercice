using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace EFGetStarted
{

    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<ProfilePicture> ProfilePictures { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=blogging.db");
    }

    public class ProfilePicture
    {
        public int ProfilePictureId { get; private set; }
        public string Name { get; set; }
        public byte[] Picture { get; set; }
    }

    public class Author
    {
        public int AuthorId { get; set; }
        [Required]
        public string Name { get; set; }
        public string LastName { get; set; }

        public int ProfilePictureId;
        public ProfilePicture ProfilePicture { get; set; }

        public List<Post> Posts { get; } = new List<Post>();
        public List<Comment> Comments { get; } = new List<Comment>();
    }

    public class Comment
    {
        public int CommentId { get; set; }
        [Required]
        public string Content { get; set; }

        public Author Author { get; set; }
    }

    public class Blog
    {
        public int BlogId { get; set; }
        [Required]
        public string Url { get; set; }

        public List<Post> Posts { get; } = new List<Post>();
    }

    public class Post
    {
        public int PostId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Content { get; set; }

        public Author Author { get; set; }
        
        public int BlogId { get; set; }
        public Blog Blog { get; set; }

        public List<Comment> Comments { get; set; } = new List<Comment>();
    }
}