﻿using System;
using System.Linq;

namespace EFGetStarted
{
    class Program
    {
        static void Main()
        {
            using (var db = new BloggingContext())
            {
                // Create
                Console.WriteLine("Inserting a new blog");
                db.Add(new Blog { Url = "http://blogs.msdn.com/adonet" });
                Console.WriteLine("Inserting a new Author");
                db.Add(new Author
                {
                    Name = "George",
                    LastName = "Bluth"
                });
                db.SaveChanges();

                // Read
                Console.WriteLine("Querying for a blog");
                var blog = db.Blogs
                    .OrderBy(b => b.BlogId)
                    .First();
                
                var author = db.Authors.OrderBy(b => b.Name).First();

                // Update
                Console.WriteLine("Updating the blog and adding a post");
                blog.Url = "https://devblogs.microsoft.com/dotnet";
                blog.Posts.Add(
                    new Post
                    {
                        Title = "Hello World",
                        Content = "I wrote an app using EF Core!",
                        Author = author
                    });
                db.SaveChanges();

                // Add Comment
                Console.WriteLine("Adding comment on a new post");
                var commentContent = "This is a great Blog!";
                var post = blog.Posts.OrderBy(p => p.Title).First();
                post.Comments.Add(
                    new Comment {
                        Content = commentContent,
                        Author = author
                    });
                db.SaveChanges();

                // Retrieving CommentContent from Author comment history
                var comment = author.Comments.OrderBy(c => c.Content).First();
                if (comment.Content == commentContent)
                {
                    Console.WriteLine("Retrieval of comment succeded");
                } else
                {
                    Console.WriteLine("Retrieval of comment failed");
                }

                // Delete
                Console.WriteLine("Delete the blog");
                db.Remove(blog);
                db.SaveChanges();
            }
        }
    }
}