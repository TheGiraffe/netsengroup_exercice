﻿using System;
using Xamarin.Forms;
using Notes.Models;
using Notes.ViewModels;

namespace Notes
{
    public partial class NotesPage : ContentPage
    {
        public NotesPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            listView.ItemsSource = await App.Database.GetNotesAsync();
        }

        async void OnNoteAddedClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NoteEntryPage
            {
                BindingContext = new NoteEntryViewModel(Navigation: Navigation, Note: new Note())
            });
        }

        async void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new NoteEntryPage
                {
                    BindingContext = new NoteEntryViewModel(Navigation: Navigation, Note: e.SelectedItem as Note)
                });
            }
        }
    }
}