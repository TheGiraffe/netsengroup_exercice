﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Notes.Models;
using Xamarin.Forms;

namespace Notes.ViewModels
{
    public class NoteEntryViewModel
    {

        public Note Note { get; set; }

        public INavigation Navigation { private get; set; }

        public ICommand SaveCommand { private set; get; }
        public ICommand DeleteCommand { private set; get; }

        public NoteEntryViewModel(INavigation Navigation, Note Note)
        {
            this.Navigation = Navigation;
            this.Note = Note;

            SaveCommand = new Command(
            execute: async () =>
            {
                Note.Date = DateTime.UtcNow;
                await App.Database.SaveNoteAsync(Note);
                await Navigation.PopAsync();
            });

            DeleteCommand = new Command(
            execute: async () =>
            {
                var answer = await App.Current.MainPage.DisplayAlert("Delete", "Are you sure you want to delete this note?", "Yes", "No");
                if (answer)
                {
                    var note = Note;
                    await App.Database.DeleteNoteAsync(note);
                    await Navigation.PopAsync();
                }
            });
        }

    }
}